package dev.galvan.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Objects;

public class BankStatements {

    private int transactionId;
    private int accountNumber;
    private int userAccountId;
    private String transactionType;
    private double transactionAmount;
    private LocalDate transactionDate;

    public BankStatements() {
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(int userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankStatements that = (BankStatements) o;
        return transactionId == that.transactionId && accountNumber == that.accountNumber && userAccountId == that.userAccountId && Double.compare(that.transactionAmount, transactionAmount) == 0 && Objects.equals(transactionType, that.transactionType) && Objects.equals(transactionDate, that.transactionDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, accountNumber, userAccountId, transactionType, transactionAmount, transactionDate);
    }

    @Override
    public String toString() {
        return "BankStatements{" +
                "transactionId=" + transactionId +
                ", accountNumber=" + accountNumber +
                ", userAccountId=" + userAccountId +
                ", transactionType='" + transactionType + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", transactionDate=" + transactionDate +
                '}';
    }
}
