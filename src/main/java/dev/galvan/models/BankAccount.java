package dev.galvan.models;

import java.util.List;
import java.util.Objects;

public class BankAccount {
    private int accountNumber;
    private int userAccountId; //this is the same id found in UserAccount class
    private double balance;
    private String accountType;
    private List<BankStatements> statementsList;

    public String getAlias() {

        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    private String alias;

    public BankAccount() {
    }

    public BankAccount(int accountId) {
        this.userAccountId = accountId;
        this.balance = 0;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(int userAccountId) {
        this.userAccountId = userAccountId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public List<BankStatements> getStatementsList() {
        return statementsList;
    }

    public void setStatementsList(List<BankStatements> statementsList) {
        this.statementsList = statementsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return accountNumber == that.accountNumber && userAccountId == that.userAccountId && Double.compare(that.balance, balance) == 0 && Objects.equals(accountType, that.accountType) && Objects.equals(statementsList, that.statementsList) && Objects.equals(alias, that.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountNumber, userAccountId, balance, accountType, statementsList, alias);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountNumber=" + accountNumber +
                ", userAccountId=" + userAccountId +
                ", balance=" + balance +
                ", accountType='" + accountType + '\'' +
                ", statementsList=" + statementsList +
                ", alias='" + alias + '\'' +
                '}';
    }
}
