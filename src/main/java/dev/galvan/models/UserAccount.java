/*  Carlos Galvan Jr 2021
*   This is the UserAccount class file that is designed to create a UserAccount object that will be stored in
*   the user_accounts table in my postgresql db
*
*
* */

package dev.galvan.models;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class UserAccount {

    //account id unique
    private int accountId;
    //this is their prefered or legal name
    private String accountHolder;
    //unique username
//    private String username;
    //email
    private String email;
    //password
    private String password;
    //shows last login and creation date
    private LocalDate lastLogin;
    private LocalDate creation;

    private List<BankAccount> openAccounts;

    public UserAccount() {
        super();
        this.lastLogin = LocalDate.now();
        this.creation = LocalDate.now();
    }

    public UserAccount(String accountHolder,  String email, String password) {
        this.accountHolder = accountHolder;
        this.email = email;
        this.password = password;
        this.lastLogin = LocalDate.now();
        this.creation = LocalDate.now();
    }

    public UserAccount(String accountHolder, String email, String password, LocalDate lastLogin, LocalDate creation, List<BankAccount> openAccounts) {
        this.accountHolder = accountHolder;
        this.email = email;
        this.password = password;
        this.openAccounts = openAccounts;
        this.lastLogin = LocalDate.now();
        this.creation = LocalDate.now();
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    //returns password as stars
    public String getPasswordEncrypt() {
        return password.replaceAll(".", "*");
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDate lastLogin) {
        if (lastLogin == null) {
            this.lastLogin = getCreation();
        } else {
            this.lastLogin = lastLogin;
        }
    }

    public LocalDate getCreation() {
        return creation;
    }

    public List<BankAccount> getOpenAccounts() {
        return openAccounts;
    }

    public void setOpenAccounts(List<BankAccount> openAccounts) {
        this.openAccounts = openAccounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return accountId == that.accountId && Objects.equals(accountHolder, that.accountHolder) && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(lastLogin, that.lastLogin) && Objects.equals(creation, that.creation) && Objects.equals(openAccounts, that.openAccounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, accountHolder, email, password, lastLogin, creation, openAccounts);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "accountId=" + accountId +
                ", accountHolder='" + accountHolder + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", lastLogin=" + lastLogin +
                ", creation=" + creation +
                ", openAccounts=" + openAccounts +
                '}';
    }
}
