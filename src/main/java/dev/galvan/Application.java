package dev.galvan;

import dev.galvan.util.AppState;

public class Application {
    public static void main(String[] args) {

        AppState.startApp();

    }
}
