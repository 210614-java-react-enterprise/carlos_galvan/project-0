package dev.galvan.util;
/*
   This file contains
 */

public class AppStateUtil {


    public static void learnMore() {
        System.out.println("Project by Carlos Galvan Jr.");
    }

    public static void welcomeMessage() {
        System.out.println("Welcome to GALVAN Bank! What business do you have with us today!");
        options();
    }
    public static void options() {
        System.out.println("[1] Log Into Account");
        System.out.println("[2] Register for an Account");
        System.out.println("[3] Learn More");
        System.out.println("ENTER 0 OR TYPE \'EXIT\' TO CLOSE APPLICATION");
    }

    //Helper funtion to make dashed line from stack overflow
    //https://stackoverflow.com/questions/36514289/how-to-print-a-line-break-in-java
    public static void dashedLine() {
        StringBuilder sb = new StringBuilder(20);
        for (int n = 0; n < 20; ++n)
            sb.append('=');
        sb.append(System.lineSeparator());
        System.out.println(sb.toString());
    }

    public static void nextStep() {
        System.out.println("Press Enter twice to continue");
    }

    static void accountMenuOptions() {
        System.out.println("[1] Make a deposit");
        System.out.println("[2] Make a withdraw");
        System.out.println("[3] View Balance");
        System.out.println("[4] Create a new Bank Account");
        System.out.println("[0] To Log off");
    }
}
