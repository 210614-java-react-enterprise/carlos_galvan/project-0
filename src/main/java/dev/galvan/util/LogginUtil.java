package dev.galvan.util;

import dev.galvan.daos.AccountAccess;
import dev.galvan.daos.AccountAccessImpl;
import dev.galvan.models.LoginPackage;
import dev.galvan.models.LoginState;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.galvan.util.AppStateUtil.options;
/*

This class contains the logInLoop which is used to handle log in
 */
public class LogginUtil {
    private static Scanner in = new Scanner(System.in);

    public static LoginState logInLoop(boolean appState) throws InterruptedException {
        System.out.println("ENTER CREDENTIALS TO LOG IN OR TYPE EXIT TO RETURN TO MAIN MENU");
        AccountAccess accountAccess = new AccountAccessImpl();
//        System.out.println("TYPE 0  TO RETURN TO MAIN MENU");
        boolean emailEntered = false;
        boolean passwordEntered = false;
        LoginPackage givenCredentials = new LoginPackage();
        System.out.println("ENTER EMAIL: ");
        while (appState) {
            String input = in.nextLine().trim();
            if (givenCredentials.getEmail() == null ) {
//
                if (verifyEmailSyntax(input)) {
                    givenCredentials.setEmail(input);
//                    emailEntered = true;
//                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("ENTER PASSWORD: ");
                } else if (!verifyEmailSyntax(input) & !input.isEmpty()) {
                    System.out.println("Invalid Email Syntax");
                }
//                System.out.println(valid);
            } else if (
                    givenCredentials.getEmail() != null && givenCredentials.getPassword() == null
//                    emailEntered && !passwordEntered
            ) {

                if (verifyPasswordSyntax(input)) {
                    givenCredentials.setPassword(input);

//                    passwordEntered = true;
                } else if (in.hasNextLine() && givenCredentials.getPassword() != null)
                {
                    System.out.println("ENTER PASSWORD: ");
                }
//                System.out.println(givenCredentials);

            } else if (givenCredentials.getEmail() != null && givenCredentials.getPassword() != null) {
                LoginState loginState = accountAccess.loginAccount(givenCredentials);

                if (loginState.isLogingStatus()) {
                    //Logged in
                    System.out.printf("Welcome %s, what business do you have today?%n", loginState.getUserAccount().getAccountHolder());
                    return loginState;
                } else {
                    System.out.println("Something went wrong? Lets try again");
                    options();
                    appState = false;
                }
//                System.out.println(givenCredentials);
            }


            if (input.toUpperCase().matches("EXIT")) {
                System.out.println("leaving log in page");
                appState = false;
            }
        }
        return null;
    }


    //helper method that verifies email syntax
//    https://mailtrap.io/blog/java-email-validation/
    static boolean verifyEmailSyntax(String input) {
        EmailValidator validator = EmailValidator.getInstance();
        // check for valid email addresses using isValid method
        return validator.isValid(input);
    }

    //Helper method to verify Password syntax
    public static boolean verifyPasswordSyntax(String password) {
        // Regex to check valid password.
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=!])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        if (password == null) {
            return false;
        }
        Matcher m = p.matcher(password);
        return m.matches();
    }
}
