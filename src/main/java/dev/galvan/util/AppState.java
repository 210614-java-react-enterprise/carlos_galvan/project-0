package dev.galvan.util;
// carlos.galvanjr@revature.net

import java.util.*;

import dev.galvan.models.LoginState;

import static dev.galvan.util.AppStateUtil.*;
import static dev.galvan.util.BankAccountUtil.accountMenu;
import static dev.galvan.util.LogginUtil.logInLoop;

import static dev.galvan.util.RegisterUtil.registerLoop;

public class AppState {
    private static Scanner in = new Scanner(System.in);

    //    public Scanner in = new Scanner(System.in);
    public static void startApp() {


        boolean appState = true;
        boolean welcome = true;

        while (appState) {
            if (welcome) {
                welcomeMessage();
                welcome = false;
            }
            try {
                int input = in.nextInt();

                if (input == 1) {
                    dashedLine();
                    LoginState loginState = logInLoop(appState);
                    accountMenu(loginState, appState);
                } else if (input == 2) {
                    dashedLine();
                    registerLoop(appState);
//                    System.out.println("You choose 2");
                } else if (input == 3) {
                    learnMore();
                    options();
                } else if (input == 0) {
                    System.out.println("BANK APPLICATION WILL NOW CLOSE | HAVE A GREAT DAY");
                    appState = false;
                } else if (input > 0 || input < 3) {

                    System.out.printf("Sorry %d is not a valid option. Try again%n", input);
                    options();
                }
            } catch (Exception e) {
                String input = in.next();
                System.out.printf("Sorry \'%s\' is not a valid option. Try again\n", input);
//                e.printStackTrace();
            }
        }
    }


}

