package dev.galvan.util;

import dev.galvan.daos.AccountAccess;
import dev.galvan.daos.AccountAccessImpl;
import dev.galvan.daos.BankTransactions;
import dev.galvan.daos.BankTransactionsImpl;
import dev.galvan.models.BankAccount;
import dev.galvan.models.BankStatements;
import dev.galvan.models.LoginState;
import dev.galvan.models.UserAccount;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static dev.galvan.util.AppStateUtil.accountMenuOptions;
import static dev.galvan.util.AppStateUtil.options;

public class BankAccountUtil {
    private static Scanner in = new Scanner(System.in);

    //This method is the main menu for users who are logged in
    static void accountMenu(LoginState loginState, boolean appState) {
        accountMenuOptions();
        BankTransactions bankTransactions = new BankTransactionsImpl();
        while (loginState.isLogingStatus()) {
            try {
                int input = in.nextInt();
                if (input == 1) {
                    loginState.getUserAccount().setOpenAccounts(chooseBankAccount(loginState.getUserAccount()));
                    options();
                } else if (input == 2) {

                } else if (input == 3) {
                    double balance = bankTransactions.checkBalance(loginState.getUserAccount());
                    System.out.println("Balance " + balance);
                    accountMenuOptions();
//                  System.out.printf("Your current balance is %d %n", balance);
                } else if (input == 4) {
                    BankAccount bankAccount = createBankAccount(loginState.getUserAccount());
                    if (loginState.getUserAccount().getOpenAccounts() == null) {
                        List<BankAccount> newAccounts = new ArrayList<>();
                        newAccounts.add(bankAccount);
                    } else if (loginState.getUserAccount().getOpenAccounts() != null) {
                        List<BankAccount> storedAccounts = loginState.getUserAccount().getOpenAccounts();
                        storedAccounts.add(bankAccount);
                    }
                    accountMenuOptions();
                } else if (input == 0) {
                    System.out.println("Logging off");
                    loginState.setLogingStatus(false);
                    options();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private static List<BankAccount> chooseBankAccount(UserAccount userAccount) {
        BankAccount bankAccount = new BankAccount();
        AccountAccess accountAccess = new AccountAccessImpl();
        BankTransactions bankTransactions = new BankTransactionsImpl();
        List<BankAccount> bankAccountList = accountAccess.getAllAssociatedBankAccounts(userAccount);
        int count = 0;
        for (BankAccount ba :
                bankAccountList) {
            System.out.printf("[%d] %s %n", count, ba.getAlias());
            count++;

        }

        System.out.println("Which account do you want to deposit to?");
        boolean run = false;
        while (!run) {
            try  {
                int response = in.nextInt();
                if (bankAccountList.get(response) != null) {
                    BankAccount current = bankAccountList.get(response);
                    System.out.println("How much do you want to deposit to " + bankAccountList.get(response).getAlias());
                    double depo = in.nextDouble();
                    BankStatements bankStatements = new BankStatements();
                    bankStatements.setAccountNumber(current.getAccountNumber());
                    bankStatements.setUserAccountId(current.getUserAccountId());
                    bankStatements.setTransactionAmount(depo);
                    bankStatements.setTransactionDate(LocalDate.now());
                    bankStatements.setTransactionType("Deposit");

                    double newBalance = bankTransactions.depositFunds(bankStatements,current.getBalance());
                    bankAccountList.get(response).setBalance(newBalance);
                    return bankAccountList;
                }
            } catch (Exception e) {
//                e.printStackTrace();
                System.out.println("Not a valid response; Try again");
            }

        }
        return bankAccountList;
    }

    private static BankAccount createBankAccount(UserAccount userAccount) {
        boolean appState = false;
        BankAccount bankAccount = new BankAccount(userAccount.getAccountId());
        System.out.println("To create a new bank account we need some information:");
        System.out.println("What type of bank account? [0 = Savings | 1 = Checking]");
        while (!appState) {

            if (bankAccount.getAccountType() == null) {
//                String responce = in.nextLine();
//                if (!responce.isEmpty()) {
                try {
                    int input = Integer.parseInt(in.next());
                    if (input == 1) {
                        bankAccount.setAccountType("Checking");
                        System.out.println("You chose Checking, Now give this account a nickname: ");
                    } else if (input == 0) {
                        bankAccount.setAccountType("Savings");
                        System.out.println("You chose Savings, Now give this account a nickname: ");
                    } else if (input != 1 || input != 0) {
                        System.out.println("Not a valid input; Try again");
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    System.out.println("Not a valid input! try again");
                }
//                }
            } else if (bankAccount.getAlias() == null) {
                String input = in.nextLine();
                if (!input.isEmpty()) {
                    System.out.printf("Do you want to name this %s account %s? [ 1 = YES or 0 = NO]%n", bankAccount.getAccountType(), input);
                    try {
                        int verify = in.nextInt();
                        if (verify == 1) {
//                            System.out.println("hello " + input);
//                            user.setAccountHolder(input);
                            bankAccount.setAlias(input.trim());
                        } else if (verify == 0) {
                            System.out.println("Lets try this again");
                        }

                    } catch (Exception e) {
                        System.out.println("[ 1 = YES or 0 = NO]");
                    }

                }

            } else if (bankAccount.getAccountType() != null && bankAccount.getAlias() != null) {
                AccountAccess accountAccess = new AccountAccessImpl();
                bankAccount = accountAccess.createNewBankAccount(bankAccount);
                if (bankAccount.getAccountNumber() > 0) {
                    System.out.println("Account creation successful!");
                    return bankAccount;
//                    appState = !appState;
                } else {
                    System.out.println("Lets try again");
                    bankAccount = new BankAccount(userAccount.getAccountId());
                }
            }
        }
//        return false;
        return bankAccount;
    }

}
