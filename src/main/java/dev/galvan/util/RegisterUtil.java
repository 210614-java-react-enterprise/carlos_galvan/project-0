package dev.galvan.util;

import dev.galvan.daos.AccountAccess;
import dev.galvan.daos.AccountAccessImpl;
import dev.galvan.models.UserAccount;

import java.util.HashSet;
import java.util.Scanner;

import static dev.galvan.util.AppStateUtil.nextStep;
import static dev.galvan.util.AppStateUtil.options;
import static dev.galvan.util.LogginUtil.verifyEmailSyntax;
import static dev.galvan.util.LogginUtil.verifyPasswordSyntax;

public class RegisterUtil {

    private static Scanner in = new Scanner(System.in);

    public static void registerLoop(boolean appState) {

        System.out.println("Register today!");
        UserAccount user = new UserAccount();
        boolean loaded = false;
        HashSet<String> tempPassword = new HashSet<>();
        while (appState) {
            String input = in.nextLine();
//            if (!loaded) {
            if (user.getEmail() == null) {
                System.out.println("Please enter your email: ");
                AccountAccess accessDao = new AccountAccessImpl();
                //if syntax is valid and email doesnt exist then add it to the user object
                boolean check = verifyEmailSyntax(input.trim()) && !accessDao.verifyAccountExist(input.trim());
                if (check) {
                    user.setEmail(input.trim());
                    nextStep();
//                            continue;
                } else if (!check && !input.isEmpty()) {
                    System.out.println("Invalid email; Try again");
                }
            } else if (user.getPassword() == null) {
                System.out.println("Create a Password: ");
//                System.out.println("your input " + input);
//                System.out.println(user);
                if (verifyPasswordSyntax(input) && !input.isEmpty()) {
//                        System.out.println("what the hell");
                    if (tempPassword.isEmpty()) {
                        tempPassword.add(input);
//                        System.out.println(tempPassword);
                        System.out.println("Re-Enter Password");
                    } else {
                        if (tempPassword.contains(input)) {
                            user.setPassword(input.trim());
                            tempPassword.clear();
                            nextStep();
//                                System.out.println("ENTER to continue");
                        } else {
                            System.out.println("Passwords dont match; Try again");
                            tempPassword.clear();
                        }
                    }
                } else if (!verifyPasswordSyntax(input) && !input.isEmpty()) {
                    System.out.println("Passwords must be at least eight characters in length (increased from six).\n" +
                            "Passwords must contain at least one lowercase letter, one uppercase letter, one number and a special character.");
                    nextStep();
                }

            } else if (user.getAccountHolder() == null) {
//                System.out.println("last round" + user);
                System.out.println("Enter your full legal name");
                if (!input.isEmpty()) {
                    System.out.printf("Is %s your full legal name? [ 1 = YES or 0 = NO]", input);
                    try {
                        int verify = in.nextInt();
                        if (verify == 1) {
                            System.out.println("hello " + input);
                            user.setAccountHolder(input);
                        } else if (verify == 0) {
                            System.out.println("Lets try this again");
                        }

                    } catch (Exception e) {
                        System.out.println("[ 1 = YES or 0 = NO]");
                    }

                }

            } else if (user.getEmail() != null && user.getPassword() != null && user.getAccountHolder() != null) {
                //This step is where the given information is upload to the Database
                AccountAccess createAccount = new AccountAccessImpl();
                boolean success = createAccount.registerUser(user);
                if (success) {
                    appState = !appState;
                    options();
                } else {
                    System.out.println("Lets try again");
                    user = new UserAccount();
                }
            }
        }
    }
}
