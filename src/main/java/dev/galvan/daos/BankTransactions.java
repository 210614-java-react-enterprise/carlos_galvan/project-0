package dev.galvan.daos;

import dev.galvan.models.BankStatements;
import dev.galvan.models.UserAccount;

/*
    This is the BankTransation interface that will correlate with the both bank_account table and bank transation table
    the created methods will serve the purpose of:
    > Depositing Funds
    > Withdrawing Funds
    > View the balance
    > Generate bank statement (transaction history)

 */
public interface BankTransactions {

    double depositFunds(BankStatements bankStatements, double balace);
    void withdrawFunds(double wDraw, UserAccount user);

    double checkBalance(UserAccount userAccount);
    boolean verifyFunds(UserAccount user, double amount);

    StringBuilder bankStatement(UserAccount user);

}
