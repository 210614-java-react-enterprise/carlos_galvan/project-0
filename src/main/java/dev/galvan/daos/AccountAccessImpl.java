package dev.galvan.daos;

import dev.galvan.models.BankAccount;
import dev.galvan.models.LoginPackage;
import dev.galvan.models.LoginState;
import dev.galvan.models.UserAccount;
import dev.galvan.util.BankAccountUtil;
import dev.galvan.util.ConnectUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AccountAccessImpl implements AccountAccess {

    //This method uses the created UserAccount Object to store the data to the DB
    @Override
    public boolean registerUser(UserAccount user) {
//        System.out.println("given email is " + email);
//        String sql = "Select auth_email from user_accounts where auth_email in ( \'" + email + "\');" ;
        String sql = "INSERT INTO public.user_accounts( " +
                " creation_date, auth_password, auth_fullname, auth_email, last_access) " +
                "VALUES ( ?, ?, ?, ?, ?);";
//        System.out.println(sql);
        try (Connection connection = ConnectUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ) {
//            preparedStatement.setInt(1, 1);
            preparedStatement.setObject(1, user.getCreation());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getAccountHolder());
            preparedStatement.setString(4, user.getEmail() );
            preparedStatement.setObject(5, user.getLastLogin() );

//            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
//            preparedStatement.executeUpdate();

            int success = preparedStatement.executeUpdate();
            if(success>0){
                return true;
            }

        } catch (SQLException throwables) {
//            throwables.printStackTrace();
            System.out.println("Something went wrong");
        }
        return false;
    }

    @Override
    public boolean verifyAccountExist(String email) {
//        System.out.println("given email is " + email);
        String sql = "Select auth_email from user_accounts where auth_email in ( \'" + email + "\');";
//        System.out.println(sql);
        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            System.out.println(rs.getMetaData());
            if (rs.next()) {
//                System.out.println("true");
                return true;
            } else {
//                System.out.println("false");
                return false;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

//        System.out.println("backup");
        return false;
    }

    @Override
    public LoginState loginAccount(LoginPackage input) {
        LoginState loginState = new LoginState();
        String sql = "SELECT user_account_id, creation_date, auth_password, auth_fullname, auth_email, last_access " +
                "FROM public.user_accounts " +
                "WHERE auth_email = \'" + input.getEmail() + "\' AND auth_password = \'" + input.getPassword() + "\';";

        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            UserAccount user = new UserAccount();
            while (rs.next()) {
//                System.out.println(rs.getMetaData());
                loginState.setLogingStatus(true);
                user.setAccountHolder(rs.getString("auth_fullname"));
                user.setEmail(rs.getString("auth_email"));
                user.setPassword(rs.getString("auth_password"));
                user.setAccountId(rs.getInt("user_account_id"));
                user.setLastLogin(rs.getObject("last_access", LocalDate.class));
            }
            loginState.setUserAccount(user);
            return loginState;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return loginState;
    }

    @Override
    public boolean verifyUserAndPassword() {
        return false;
    }

    @Override
    public BankAccount createNewBankAccount(BankAccount bankAccount) {
        String sql = "INSERT INTO public.bank_account( " +
                " balance, account_type, user_account_id, nickname) " +
                "VALUES ( ?, ?, ?, ?);";
        try (Connection connection = ConnectUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
//            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(1, (int) (bankAccount.getBalance() * 100));
            preparedStatement.setString(2, bankAccount.getAccountType());
            preparedStatement.setInt(3, bankAccount.getUserAccountId());
            preparedStatement.setString(4, bankAccount.getAlias());

//            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
//            preparedStatement.executeUpdate();

            preparedStatement.executeUpdate();

            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                bankAccount.setAccountNumber(rs.getInt("account_number"));
            }
        return bankAccount;
        } catch (SQLException throwables) {
//            throwables.printStackTrace();
            System.out.println("Something went wrong");
        }
        return bankAccount;
    }

    @Override
    public List<BankAccount> getAllAssociatedBankAccounts(UserAccount userAccount) {
        List<BankAccount> bankAccountList = new ArrayList<>();
        String sql = "SELECT account_number, balance, account_type, user_account_id, nickname " +
                "FROM public.bank_account " +
                "WHERE user_account_id = \'" + userAccount.getAccountId() + "\';";

        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
//            UserAccount user = new UserAccount();

            while (rs.next()) {
                BankAccount bankAccount = new BankAccount();
                bankAccount.setAccountNumber(rs.getInt("account_number"));
                bankAccount.setUserAccountId(rs.getInt("user_account_id"));

                bankAccount.setBalance(((float) rs.getInt("balance")) / 100);
                bankAccount.setAccountType(rs.getString("account_type"));
                bankAccount.setAlias(rs.getString("nickname"));
                bankAccountList.add(bankAccount);
            }
            return bankAccountList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
