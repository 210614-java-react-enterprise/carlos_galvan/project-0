package dev.galvan.daos;

import dev.galvan.models.BankStatements;
import dev.galvan.models.UserAccount;
import dev.galvan.util.ConnectUtil;

import java.sql.*;

public class BankTransactionsImpl implements BankTransactions{
    @Override
    public double depositFunds(BankStatements bankStatements, double balance) {
        int success = 0;
        boolean greenLight = false;
        String sql = "INSERT INTO public.bank_transaction( " +
                " account_number, transaction_type, base_amount, transaction_date, user_account_id) " +
                " VALUES (?, ?, ?, ?, ?);";
        String balanceQuery = "UPDATE public.bank_account " +
                "SET balance = ? " +
                "WHERE user_account_id = ? AND account_number =  ?;";
        try (Connection connection = ConnectUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, bankStatements.getAccountNumber());
            preparedStatement.setString(2, bankStatements.getTransactionType());
            preparedStatement.setDouble(3, (int) ( bankStatements.getTransactionAmount() * 100));
            preparedStatement.setObject(4, bankStatements.getTransactionDate());
            preparedStatement.setInt(5, bankStatements.getUserAccountId());

            preparedStatement.executeUpdate();
            System.out.println("Loading..");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

//        if (greenLight) {
            try (Connection connection = ConnectUtil.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(balanceQuery);
            ) {
                preparedStatement.setInt(1,(int) ((bankStatements.getTransactionAmount() + balance) * 100));
                preparedStatement.setInt(2, bankStatements.getUserAccountId());
                preparedStatement.setInt(3, bankStatements.getAccountNumber());

                System.out.println("Loading...");
                success = preparedStatement.executeUpdate();
                if (success > 0) {
                    return bankStatements.getTransactionAmount();
                } else {
                    System.out.println("Something went wrong; Try againt");
                    return bankStatements.getTransactionAmount();
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        return balance;
    }

    @Override
    public void withdrawFunds(double wDraw, UserAccount user) {

    }

    @Override
    public double checkBalance(UserAccount userAccount) {
    String sql = "SELECT balance FROM public.bank_account WHERE user_account_id = " + userAccount.getAccountId()+ ";";
        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            while (rs.next()) {
                double total = rs.getInt("balance") * 0.01;
                System.out.println(total);
                return total;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean verifyFunds(UserAccount user, double amount) {
        return false;
    }

    @Override
    public StringBuilder bankStatement(UserAccount user) {
        return null;
    }
}
